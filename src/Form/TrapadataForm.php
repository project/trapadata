<?php

namespace Drupal\trapadata\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form.
 */
class TrapadataForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trapadata_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('trapadata.settings');
    // Page title field.
    $form['page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Trapadata management page title:'),
      '#default_value' => $config->get('trapadata.page_title'),
      '#description' => $this->t('Give your Trapadata management page a title.'),
    ];
    // Setting 1 field.
    $form['setting_1'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Setting 1 for Trapadata management:'),
      '#default_value' => $config->get('trapadata.setting_1'),
      '#description' => $this->t('Write sample value.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('trapadata.settings');
    $config->set('trapadata.setting_1', $form_state->getValue('setting_1'));
    $config->set('trapadata.page_title', $form_state->getValue('page_title'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'trapadata.settings',
    ];
  }

}
